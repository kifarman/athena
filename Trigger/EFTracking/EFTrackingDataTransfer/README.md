# Set of utilities to facilitate data transfer operations that are EFTracking specific

# Benchmarks
The package contains also a set of benchmark programs, they exercise various operations related to the data transfer.
Current benchmark measures time needed to write and read sets of SpacePoint to or from SpacePointContainer.
There are currently 5 scenarios in 5 different sizes.
